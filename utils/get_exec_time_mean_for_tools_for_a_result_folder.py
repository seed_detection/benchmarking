
import sys
import json
import os

def get_networks_with_results(folder):
        networks = list()
        directories = os.listdir(folder)
        for dir_ in directories:
                if os.path.exists(f"{folder}/{dir_}/results.json"):
                        networks.append(dir_)
        return networks

def run(folder):
	output = {'netseed':{'mean':0, 'sum':0, 'nb_network':0}, 
		  'netseed_asp':{'mean':0, 'sum':0, 'nb_network':0}, 
		  'precursor':{'mean':0, 'sum':0, 'nb_network':0},
		  'predator':{'mean':0, 'sum':0, 'nb_network':0}}
	networks_list = get_networks_with_results(folder)
	for network in networks_list:
		with open(f'{folder}/{network}/results.json', 'r') as f:
			data = json.load(f)	
		for tool in data:
			output[tool]['sum'] += float(data[tool]['time'])
			output[tool]['nb_network'] += 1
	for tool in output:
		if output[tool]['nb_network'] != 0:
			output[tool]['mean'] = output[tool]['sum']/output[tool]['nb_network']
	return output

if __name__ == '__main__':
	folder = sys.argv[1]
	output =run(folder)
	print(output)

