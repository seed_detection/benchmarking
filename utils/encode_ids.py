from padmet.utils.sbmlPlugin import convert_to_coded_id
import sys
with open(sys.argv[1], 'r') as input_:
    with open(sys.argv[2], 'w') as output:
        for line in input_:
            output.write(convert_to_coded_id(line))