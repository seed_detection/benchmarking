#!/bin/bash

# ------------- GLOBAL VARIABLES ------------- #

BENCHMARKING_DIR_PATH=$1
BIOCYC_PATH=$2
TIERS_FILE=$3
REQUIRED_DAT_FILES=("classes.dat" "compounds.dat" "proteins.dat" "reactions.dat" "enzrxns.dat" "pathways.dat")


# ------------- FUNCTIONS DEFINITIONS ------------- #

move_to_tiers(){
        file=$1
        folder=$2
        mv $file "$folder/$sile"

}


# ------------- MAIN ------------- #

mapfile -t  tiers1< <(jq -r '.tiers1 | @sh' $TIERS_FILE)
mapfile -t  tiers2< <(jq -r '.tiers2 | @sh' $TIERS_FILE)


folder_names=$(ls $BIOCYC_PATH)
mkdir "$BIOCYC_PATH/tiers1" "$BIOCYC_PATH/tiers2" "$BIOCYC_PATH/tiers3" "$BIOCYC_PATH/to_sort" "$BIOCYC_PATH/problems"

for name in ${folder_names[@]}; do
        folder="$BIOCYC_PATH/$name"
	echo "SPECIE NAME : $name"
        # Test if all required files for padmet are presents
	dat_files_problem=false
	for dat_file in ${REQUIRED_DAT_FILES[@]}; do
		echo "DAT FILE TESTED  : $dat_file"
		 
		if [[ ! -f "$folder/$dat_file"  ]] ; then 
			echo "PROBLEM TRUE"
			dat_files_problem=true
		fi
	done

        # If a required file is missing
	if [ $dat_file_problems ]; then
		move_to_tiers $folder "$BIOCYC_PATH/problems"

        elif [[ -f "$folder/pubs.dat" ]]; then
                # `\K` to escape the match and get only what is after 
                specie_name=$(grep -Po '^# Species:\s\K.*' "$folder/pubs.dat")

                if [[ ${tiers1[@]} =~ '${specie_name}'  ]]; then
                        move_to_tiers $folder "$BIOCYC_PATH/tiers1"

                elif [[ ${tiers2[@]} =~ '${specie_name}'  ]]; then
                        move_to_tiers $folder "$BIOCYC_PATH/tiers2"
                else
			new_name=$(bash $BENCHMARKING_DIR_PATH/encode_string.sh "$specie_name")
			echo "NEW NAME : $new_name"	
			new_folder="$BIOCYC_PATH/$new_name"
			mv $folder $new_folder
                        move_to_tiers $new_folder "$BIOCYC_PATH/tiers3"
                fi
        # If specie have no 'pubs.dat' file, the name is not present : need to sort manually
        else
                move_to_tiers $folder "$BIOCYC_PATH/to_sort"
        fi

done

