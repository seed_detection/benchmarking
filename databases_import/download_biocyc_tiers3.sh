#!/bin/bash

#BATCH --job-name=download_biocyc_tiers3   # Job name
#SBATCH --mail-type=END,FAIL         # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr # Where to send mail
#SBATCH --time=05:00:00             # Time limit hrs:min:sec
#SBATCH --nodes=1                   # Use one node
#SBATCH --ntasks=1                  # Run a single task
#SBATCH --output=/beegfs/mbolteau/seed_detection/logs/download_biocyc_tiers3_array_%A-%a.out    # Standard output and error log
#SBATCH --array=1-1%100 # job array index # or -t 1-3%3
#SBATCH --mem-per-cpu=1gb


# ------------- GLOBAL VARIABLES ------------- #

BIOCYC_USERNAME=$1
BIOCYC_PASSWORD=$2
BIOCYC_DIR_NAME=$3
TIERS_3_SPECIES=$4
BENCHMARKING_DIR_PATH="/beegfs/mbolteau/seed_detection/benchmarking"
MODELS_EXTRACTED_NUMBER=1000


# ------------- MAIN ------------- #


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

echo “============================ “

source /home/mbolteau/miniconda3/etc/profile.d/conda.sh
conda activate benchmarking

cd $BIOCYC_DIR_NAME

specie_data=`cat $TIERS_3_SPECIES | head -n ${SLURM_ARRAY_TASK_ID} | tail -n 1 `
specie_name=`echo $specie_data | sed 's/\s\S*$//g'`
url=`echo $specie_data | rev | cut -d$' ' -f 1 | rev`
specie_name_encoded=`bash $BENCHMARKING_DIR_PATH/encode_string.sh "$specie_name"`
echo SPECIE $specie_name

mkdir $specie_name_encoded $specie_name_encoded/dat
curl --silent --location --insecure --user "$BIOCYC_USERNAME:$BIOCYC_PASSWORD" -o "$specie_name_encoded/$specie_name_encoded.tar.gz" $url 
size=`du -k $specie_name_encoded/$specie_name_encoded.tar.gz | cut -f1`
echo  SIZE: $size
if (( $size > 5  )) ; then
	echo OK
	tar -xzf $specie_name_encoded/$specie_name_encoded.tar.gz -C $specie_name_encoded/dat
	rm $specie_name_encoded/$specie_name_encoded.tar.gz
	version=`ls -d $specie_name_encoded/dat/*/*/ | rev | cut -d'.' -f2 | rev`
	padmet pgdb_to_padmet --pgdb $specie_name_encoded/dat/*/*/data --output $specie_name_encoded/$specie_name_encoded.padmet --version $version --db $specie_name_encoded
	padmet sbmlGenerator --padmet $specie_name_encoded/$specie_name_encoded.padmet --output $specie_name_encoded/$specie_name_encoded.sbml
	tar -czf $specie_name_encoded/dat.tar.gz $specie_name_encoded/dat && rm -R $specie_name_encoded/dat
	tar -czf $specie_name_encoded/$specie_name_encoded.padmet.tar.gz $specie_name_encoded/$specie_name_encoded.padmet && rm $specie_name_encoded/$specie_name_encoded.padmet
else
	echo KO
	echo  $specie_name_encoded >> $BIOCYC_DIR_NAME/empty_archives.txt
	rm -R $specie_name_encoded

fi





