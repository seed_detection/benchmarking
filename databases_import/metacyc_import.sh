#!/bin/bash

# ------------- GLOBAL VARIABLES ------------- #

BENCHMARKING_DIR_PATH=$1
BIOCYC_USERNAME=$2    
BIOCYC_PASSWORD=$3   
CURRENT_DIR=$(pwd)
METACYC_DIR_NAME="metacyc"
BIGG_DIR_NAME="bigg"
BIGG_E_COLI_ID="iML1515" 
E_COLI_DEGRADED_DIR_NAME="e_coli_degraded"


# ------------- FUNCTIONS DEFINITIONS ------------- #
:'
bigg_e_coli_import(){
    dir=$BIGG_DIR_NAME
    mkdir $dir $dir/data $dir/sbml
    
    curl -s -o $dir/data/db_version.txt 'http://bigg.ucsd.edu/api/v2/database_version'

    cd $dir/sbml
    curl -s -O http://bigg.ucsd.edu/static/models/$BIGG_E_COLI_ID.xml

    cd $CURRENT_DIR
}


degrade_e_coli(){
    sbml_input_path=$1
    output_directory=$2
    python3 $BENCHMARKING_DIR_PATH/degrade_sbml.py $sbml_input_path $output_directory
}
'

metacyc_import(){
    dir=$METACYC_DIR_NAME
    mkdir $dir $dir/data $dir/pathways
    cd $dir/data
    curl -s --insecure --user "$BIOCYC_USERNAME:$BIOCYC_PASSWORD" -o meta.tar.gz https://brg-files.ai.sri.com/public/dist/meta.tar.gz 
    tar -xzf meta.tar.gz
    rm meta.tar.gz
    version=$(ls -d */ | sed -e "s/\///g")
    padmet pgdb_to_padmet --pgdb $version/data/ --version $version --db metacyc --output metacyc_$version.padmet

    cd $CURRENT_DIR
    python3 $BENCHMARKING_DIR_PATH/databases_import/metacyc_treatement.py $dir/data/metacyc_$version.padmet $dir/pathways
}



 
# ------------- MAIN ------------- #

conda update -y -n base -c defaults conda
conda create -y -n benchmarking python=3.8 jq git
conda init bash
source ~/.bashrc

conda activate benchmarking
python3 -m pip install --upgrade pip
git -C ../utils/ clone https://github.com/AuReMe/padmet.git -q
python3 -m pip install ../utils/padmet/.


#bigg_e_coli_import
#degrade_e_coli "$BIGG_DIR_NAME/sbml/$BIGG_E_COLI_ID.xml" $E_COLI_DEGRADED_DIR_NAME
metacyc_import
