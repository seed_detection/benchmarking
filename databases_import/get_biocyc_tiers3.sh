#!/bin/bash

# CALL : `bash get_biocyc_tiers3.sh <tier1 and 2 JSON path> <BioCyc HTML page path> <output filename>`

# ------------- GLOBAL VARIABLES ------------- #

TIERS_FILE=$1
HTML_PAGE=$2
OUTPUT_FILENAME=$3

# ------------- MAIN ------------- #



mapfile -t  tiers1< <(jq -r '.tiers1 | @sh' $TIERS_FILE)
mapfile -t  tiers2< <(jq -r '.tiers2 | @sh' $TIERS_FILE)
tiers1_2=("${tiers1[@]}" "${tiers2[@]}")

count=`xmllint --html $HTML_PAGE --xpath "count(//html/body/table[last()]/tr)" 2> /dev/null `

for ((i=1; i <= $count; i++)); do
    specie=$(xmllint --html $HTML_PAGE --xpath '//html/body/table[last()]/tr['"$i"']/td[1]/descendant-or-self::*/text()'  2> /dev/null)
    if [[ ! "${tiers1_2[@]}" =~ $specie ]]; then
        url=$(xmllint --html $HTML_PAGE --xpath '//html/body/table[last()]/tr['"$i"']/td[2]/a/@href'  2> /dev/null | sed 's/href="\(.*\)"/\1/')
        echo -e ${specie} "\t" ${url} >> $OUTPUT_FILENAME
    fi
done
