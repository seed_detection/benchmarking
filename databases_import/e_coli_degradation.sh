#!/bin/bash

# USAGE : bash e_coli_degradation.sh <benchmarking directory path> <SBML input file path> <output directory>


# ------------- GLOBAL VARIABLES ------------- #

benchmarking_dir_path=$1
sbml_input_path=$2
output_directory=$3


python3 $benchmarking_dir_path/python/e_coli_degradation.py $sbml_input_path $output_directory



