#!/usr/bin/env python

import sys
import os
import json
from padmet.classes.padmetRef import PadmetRef
from padmet.classes.padmetSpec import PadmetSpec
from padmet.utils.connection.sbmlGenerator import padmet_to_sbml

PADMET_REF_PATH     = sys.argv[1]
OUTPUT_DIRECTORY    = sys.argv[2]


def metacyc_pwy_import():    
    p_ref = PadmetRef(PADMET_REF_PATH)
    all_pwys = set(p_ref.getPathways())

    # Remove super pathways
    pwy_to_del = set()
    for pwy in all_pwys:
        pwy_list = set([rlt.id_out for rlt in p_ref.dicOfRelationOut[pwy] if rlt.type == "is_in_pathway" and p_ref.dicOfNode[rlt.id_in].type == "pathway"])
        pwy_to_del.update(pwy_list)
    all_pwys = all_pwys.difference(pwy_to_del)
    
    for pwy in all_pwys:
        os.mkdir(f'{OUTPUT_DIRECTORY}/{pwy}')

        # Create the SBML file
        p_spec = PadmetSpec()
        rxn_list = [rlt.id_in for rlt in p_ref.dicOfRelationOut[pwy] if rlt.type == "is_in_pathway" and p_ref.dicOfNode[rlt.id_in].type == "reaction"]
        for rxn_id in rxn_list:
            p_spec.copyNode(p_ref, rxn_id)
        padmet_to_sbml(p_spec, f'{OUTPUT_DIRECTORY}/{pwy}/{pwy.lower()}.sbml', sbml_lvl=2, verbose=False)

        # Get inputs-compounds of the pathway
        input_compounds = None
        pwy_object = p_ref.dicOfNode[pwy]
        if 'INPUT-COMPOUNDS' in pwy_object.misc:
            input_compounds = {'inputs_compounds': pwy_object.misc['INPUT-COMPOUNDS'][0].split(',')}
        if input_compounds:
            with open(f'{OUTPUT_DIRECTORY}/{pwy}/inputs_compounds.json', 'w') as json_file:
                json.dump(input_compounds, json_file)
        



if __name__ == '__main__':   
    metacyc_pwy_import() 

    
