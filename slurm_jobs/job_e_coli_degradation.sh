#!/bin/bash

#SBATCH --job-name=e_coli_degradation    # Job name
#SBATCH --mail-type=END,FAIL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr     # Where to send mail
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=30gb                     # Job memory request
#SBATCH --time=00:30:00               # Time limit hrs:min:sec
#SBATCH -o /beegfs/mbolteau/seed_detection/logs/e_coli_degradation_%j.out   # Standard output and error log
#SBATCH -e /beegfs/mbolteau/seed_detection/logs/e_coli_degradation_%j.err


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

echo “============================ “

################# START


source /home/mbolteau/miniconda3/etc/profile.d/conda.sh
conda activate benchmarking

cd /beegfs/mbolteau/seed_detection/benchmarking/python

bash /beegfs/mbolteau/seed_detection/benchmarking/e_coli_degradation.sh /beegfs/mbolteau/seed_detection/benchmarking /beegfs/mbolteau/seed_detection/databases/bigg/sbml/iML1515.xml /beegfs/mbolteau/seed_detection/databases/e_coli_degraded

 
