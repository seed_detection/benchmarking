#!/bin/bash

#SBATCH --job-name=run_n2pcomp_with_targets    # Job name
#SBATCH --mail-type=END,FAIL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr     # Where to send mail
#SBATCH --time=05:00:00             # Time limit hrs:min:sec
#SBATCH --nodes=1                   # Use one node
#SBATCH --ntasks=1                  # Run a single task
#SBATCH --output=/beegfs/mbolteau/seed_detection/logs/run_n2pcomp_with_targets_%A-%a.out    # Standard output and error log
#SBATCH --array=1-107%107 # job array index # or -t 1-3%3
#SBATCH --mem-per-cpu=1gb


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

echo “============================ “

################# START

NETWORK_DIR=$1
OUTPUT=$2
CONFIG_FILE=$3

source /home/mbolteau/miniconda3/etc/profile.d/conda.sh
conda activate benchmarking


network=`ls $NETWORK_DIR | head -n ${SLURM_ARRAY_TASK_ID} | tail -n 1`

network_name="$(basename -- $network)"
network_name="${network_name%.*}"

targets=$(python /beegfs/mbolteau/seed_detection/benchmarking/python/get_cpds_of_biomass_reaction.py $NETWORK_DIR/$network 10)

echo  $targets

formatted_targets=$(echo $targets | tr -d ,\[\]\' )

echo $formatted_targets

srun mkdir -p /beegfs/mbolteau/tmp/$SLURM_JOB_ID
echo "dir created"

cd /beegfs/mbolteau/tmp/$SLURM_JOB_ID


python -m n2pcomp $NETWORK_DIR/$network --output $OUTPUT/$network_name --conf-file $CONFIG_FILE --targets $formatted_targets 

cd /beegfs/mbolteau/
rm -R /beegfs/mbolteau/tmp/$SLURM_JOB_ID

