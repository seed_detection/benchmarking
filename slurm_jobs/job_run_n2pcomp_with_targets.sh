#!/bin/bash

#SBATCH --job-name=run_n2pcomp    # Job name
#SBATCH --mail-type=END,FAIL          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr     # Where to send mail
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=30gb                     # Job memory request
#SBATCH --time=10:00:00               # Time limit hrs:min:sec
#SBATCH -o /beegfs/mbolteau/seed_detection/logs/run_n2pcomp_%j.out   # Standard output and error log
#SBATCH -e /beegfs/mbolteau/seed_detection/logs/run_n2pcomp_%j.err


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

echo “============================ “

################# START


NETWORK=$1
OUTPUT=$2
CONFIG_FILE=$3
TARGETS=("$@")
unset 'TARGETS[0]' 'TARGETS[1]' 'TARGETS[2]'

source /home/mbolteau/miniconda3/etc/profile.d/conda.sh
conda activate benchmarking

cd /beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp

python -m n2pcomp $NETWORK --output $OUTPUT --conf-file $CONFIG_FILE -t ${TARGETS[*]}

