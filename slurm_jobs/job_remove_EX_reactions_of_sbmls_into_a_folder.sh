#!/bin/bash


#BATCH --job-name=remove_EX   # Job name
#SBATCH --mail-type=END,FAIL         # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr # Where to send mail
#SBATCH --time=05:00:00             # Time limit hrs:min:sec
#SBATCH --nodes=1                   # Use one node
#SBATCH --ntasks=1                  # Run a single task
#SBATCH --output=/beegfs/mbolteau/seed_detection/logs/remove_EX_%A-%a.out    # Standard output and error log
#SBATCH --array=1-100%100 # job array index # or -t 1-3%3
#SBATCH --mem-per-cpu=1gb


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

echo “============================ “

source /home/mbolteau/miniconda3/etc/profile.d/conda.sh
conda activate benchmarking

dir_path=$1
output_dir_path=$2

pwy=`ls $dir_path | head -n ${SLURM_ARRAY_TASK_ID} | tail -n 1`

name="$(basename -- $pwy)"
name="${name%.*}"
output_name=$name"_without_EX_reactions.xml"

python /beegfs/mbolteau/seed_detection/benchmarking/python/remove_EX_reactions.py "$dir_path/$pwy" "$output_dir_path/$output_name"


