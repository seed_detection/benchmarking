#!/bin/bash

#SBATCH --job-name=benchmarking    # Job name
#SBATCH --mail-type=END,FAIL,BEGIN          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr     # Where to send mail	
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=30gb                     # Job memory request
#SBATCH --time=24:00:00               # Time limit hrs:min:sec
#SBATCH -o benchmarking_%j.out   # Standard output and error log
#SBATCH -e benchmarking_%j.err


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

################# START

cd /beegfs/mbolteau/seed_detection/databases
bash ../benchmarking/databases_import/metacyc_import.sh "/beegfs/mbolteau/seed_detection/benchmarking" "biocyc-flatfiles" "data-20541"


