#!/bin/bash


#BATCH --job-name=meta_comp   # Job name
#SBATCH --mail-type=END,FAIL         # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr # Where to send mail
#SBATCH --time=05:00:00             # Time limit hrs:min:sec
#SBATCH --nodes=1                   # Use one node
#SBATCH --ntasks=1                  # Run a single task
#SBATCH --output=/beegfs/mbolteau/seed_detection/logs/meta_comp_array_%A-%a.out    # Standard output and error log
#SBATCH --array=1-1000%100 # job array index # or -t 1-3%3
#SBATCH --mem-per-cpu=1gb


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

echo “============================ “

source /home/mbolteau/miniconda3/etc/profile.d/conda.sh
conda activate free_cofactors

metacyc_dir_path=$1

pwy=`ls $metacyc_dir_path | head -n ${SLURM_ARRAY_TASK_ID} | tail -n 1`

cd /beegfs/mbolteau/seed_detection/results/whole_network/metacyc_pathways


python3 -m n2pcomp --predator-netseed-asp --conf-file /beegfs/mbolteau/seed_detection/config/cofactors_free_meta_comp_config.yaml --export $pwy $metacyc_dir_path/$pwy/*.sbml

