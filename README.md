# Benchmarking for seed detection

This repository will be used for data import for the benchmarking of the seed detection comparison. All code used during the project for the benchmarking is in this repository.


The actual architecture of the folder in Plafrim is :

```
/beegfs/mbolteau/seed_detection/
├── benchmarking
│   ├── README.md
│   ├── analyze
│   ├── config
│   ├── databases_import
│   ├── python
│   ├── requirements.txt
│   ├── slurm_jobs
│   ├── test_data
│   ├── todel
│   └── utils
├── computation_time_netseed_vs_netseed-asp
├── databases
│   ├── bigg
│   ├── biocyc
│   ├── e_coli_degraded
│   └── metacyc
├── logs
├── metacyc_analyse
├── programs
│   ├── MeneTools
│   ├── netseed
│   ├── precursor
│   ├── predator
│   └── tools-comparison
├── results
│   ├── target_specific
│   │   ├── bigg
│   │   ├── e_coli
│   │   └── e_coli_degraded
│   └── whole_network
│       ├── biocyc
│       └── metacyc_pathways
├── test
└── utils
```

* `benchmarking` folder contains all code for import databases and process analysis
* `computation_time_netseed_vs_netseed-asp` folder for the analyze of this experiment
* `databases` folder contains networks imported from databases
* `logs` folder contains logs created during the  run of jobs on Plafrim
* `metacyc_analyse` folder for the analyze of this experiment
* `programs` folder contains programs required for the analysis
* `results` folder contains results for each experiment given by N2PComp program
* `test` contains data used for simple test
* `utils` folder for utilitaries


# Requirements

* requests>=2.26.0
* seaborn>=0.11.1
* python-libsbml>=5.19.0


