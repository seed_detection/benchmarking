#!/bin/bash


networks=(e_coli_core  iCHOv1  iCN900  iEcDH1_1363  iIS312  iIT341  iJN1463  iJN678  iJR904  iLB1027_lipid  iLJ478  iMM1415  iPC815  iSbBS512_1146  iYS1720  Recon3D)


for network in ${networks[*]}; do
	echo $network "..."
	sbatch /beegfs/mbolteau/seed_detection/slurm_jobs/job_run_n2pcomp.sh "/beegfs/mbolteau/seed_detection/databases/bigg/sbml_without_EX_reactions/"$network"_without_EX_reactions.xml" /beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results/$network /beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/config/config.yaml 

done
