#!/bin/bash

# ------------- GLOBAL VARIABLES ------------- #
BENCHMARKING_DIR_PATH=$1
TIERS3_PATH=$2
INITIAL_DIR_PATH=$(pwd)

# ------------- MAIN ------------- #

species=$(ls $TIERS3_PATH)

for specie in ${species[@]}; do
	echo $specie
	current_path="$TIERS3_PATH/$specie"
	if [[ ! -f "$current_path/dat.tar.gz" ]]; then	# Permit to pass already treated species
		database_name=$(grep -Po '^# Database:\s\K.*' "$current_path/pubs.dat")
		coded_database_name=$(bash $BENCHMARKING_DIR_PATH/encode_string.sh "$database_name")
        	database_version=$(grep -Po '^# Version:\s\K.*' "$current_path/pubs.dat")
		mkdir "$current_path/dat"
		to_move=$(ls $current_path | egrep -v '^dat') 
		for file in ${to_move[@]}; do mv "$current_path/$file" "$current_path/dat/" ; done
		padmet pgdb_to_padmet --pgdb "$current_path/dat" --output "$current_path/$specie.padmet" --version $database_version --db $coded_database_name
		padmet sbmlGenerator --padmet "$current_path/$specie.padmet" --output "$current_path/$specie.sbml"
		cd $current_path	
		tar -czf "dat.tar.gz" ./dat/* && rm -R "./dat"
		tar -czf "$specie.padmet.tar.gz" "$specie.padmet" && rm "$specie.padmet"
		cd $INITIAL_DIR_PATH
	fi
done

