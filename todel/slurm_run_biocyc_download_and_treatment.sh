#!/bin/bash

#SBATCH --job-name=biocyc_download_and_treatment    # Job name
#SBATCH --mail-type=END,FAIL,BEGIN          # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=mathieu.bolteau@inria.fr     # Where to send mail	
#SBATCH --ntasks=1                    # Run on a single CPU
#SBATCH --mem=30gb                     # Job memory request
#SBATCH --time=24:00:00               # Time limit hrs:min:sec
#SBATCH -o biocyc_download_and_treatment_%j.out   # Standard output and error log
#SBATCH -e biocyc_download_and_treatment_%j.err


echo “=====my job information ==== “

echo “Node List: ” $SLURM_NODELIST
echo “my jobID: ” $SLURM_JOB_ID
echo “Partition: ” $SLURM_JOB_PARTITION
echo “submit directory:” $SLURM_SUBMIT_DIR
echo “submit host:” $SLURM_SUBMIT_HOST
echo “In the directory: $PWD”
echo “As the user: $USER”

echo “============================ “

################# START

benchmarking_dir_path="../benchmarking"
biocyc_dir_name="biocyc"

biocyc_username="biocyc-flatfiles"
biocyc_password="data-20541"

cd /beegfs/mbolteau/seed_detection/databases
bash $benchmarking_dir_path/download_biocyc.sh $biocyc_username $biocyc_password $biocyc_dir_name
bash $benchmarking_dir_path/biocyc_tiers3_treatment.sh $benchmarking_dir_path $biocyc_dir_name


