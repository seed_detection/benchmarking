#!/bin/bash

# ------------- GLOBAL VARIABLES ------------- #

BIOCYC_USERNAME=$1
BIOCYC_PASSWORD=$2
BIOCYC_DIR_NAME=$3


# ------------- MAIN ------------- #

mkdir $BIOCYC_DIR_NAME
curl -C -  --insecure --user "$BIOCYC_USERNAME:$BIOCYC_PASSWORD" -o "$BIOCYC_DIR_NAME/biocyc-flatfiles.tar.gz" https://brg-files.ai.sri.com/public/dist/biocyc-flatfiles.tar.gz 
exit_status=$?
echo "EXIT STATUS AFTER BIOCYC DOWNLOAD : " $exit_status
if [[ "$exit_status" == "0" ]]; then
	tar -xzf "$BIOCYC_DIR_NAME/biocyc-flatfiles.tar.gz" -C $BIOCYC_DIR_NAME
	exit_status_tar=$?
	echo "EXIT STATUS AFTER TAR: " $exit_status_tar
fi


