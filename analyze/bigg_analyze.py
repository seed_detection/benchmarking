#!/usr/bin/env python


import os
import json
import sys
import csv
import random
import pathlib
from padmet.utils.sbmlPlugin import convert_from_coded_id
from menetools.meneseed import run_meneseed
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
from libsbml import *

DATABASE_DIR = '/beegfs/mbolteau/seed_detection/databases/bigg/sbml'
RESULTS_DIR = '/beegfs/mbolteau/seed_detection/results/target_specific/bigg'
#RESULTS_DIR = '/beegfs/mbolteau/seed_detection/test/bigg'


def compute_precursor_union_and_intersection(data):
	solutions_list = list()
	for solution in data:
		solutions_list.append(set(data[solution]))
	union = set().union(*solutions_list)
	intersection = set().intersection(*solutions_list)
	return union, intersection

def get_compounds(filename):
	document = readSBML(filename)
	compounds = set([cpd.getId() for cpd in document.getModel().getListOfSpecies()])
	return compounds


def confusion_matrix(all_, truth, pred):
        true_pos = pred.intersection(truth)
        true_neg = (all_.difference(pred)).intersection(all_.difference(truth))
        false_pos = pred.difference(truth)
        false_neg = (all_.difference(pred)).intersection(truth)
        return true_pos, true_neg, false_pos, false_neg

def get_external_compounds(filename):
	seeds =  run_meneseed(filename)
	return seeds['seeds']


def get_networks_with_results():
	networks = list()
	directories = os.listdir(RESULTS_DIR)
	for dir_ in directories:
		if os.path.exists(f"{RESULTS_DIR}/{dir_}/results.json"):
			networks.append(dir_)
	return networks


def run_analyze():
	output = {'predator': {'true_pos': 0, 'true_neg': 0, 'false_pos': 0, 'false_neg': 0},
		  'precursor': {'true_pos': 0, 'true_neg': 0, 'false_pos': 0, 'false_neg': 0}}
	networks = get_networks_with_results()
	for network in networks:
		network_name = network.split('_without_')[0]
		network_compounds = get_compounds(f'{DATABASE_DIR}/{network_name}.xml')
		ext_compounds =  get_external_compounds(f'{DATABASE_DIR}/{network_name}.xml')
		with open(f'{RESULTS_DIR}/{network}/results.json', 'r') as f:
			network_results = json.load(f)
		precursor_union, _ = compute_precursor_union_and_intersection(network_results['precursor']['solutions'])
		predator_union = network_results['predator']['solutions']['union']
		t_pos, t_neg, f_pos, f_neg = confusion_matrix(network_compounds, ext_compounds, precursor_union)
		output['precursor']['true_pos'] += len(t_pos)
		output['precursor']['true_neg'] += len(t_neg)
		output['precursor']['false_pos'] += len(f_pos)
		output['precursor']['false_neg'] += len(f_neg)
		t_pos, t_neg, f_pos, f_neg = confusion_matrix(network_compounds, ext_compounds, set(predator_union))
		output['predator']['true_pos'] += len(t_pos)
		output['predator']['true_neg'] += len(t_neg)
		output['predator']['false_pos'] += len(f_pos)
		output['predator']['false_neg'] += len(f_neg)

	return output

if __name__ == '__main__':
	results = run_analyze()
	print(results)

