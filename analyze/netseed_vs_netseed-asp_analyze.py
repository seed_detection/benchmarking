
from libsbml  import *
import json
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
import os
import math

DATABASE_DIR = '/beegfs/mbolteau/seed_detection/databases/bigg/sbml_without_EX_reactions'
RESULTS_DIR = '/beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results' 

#networks_list = ['e_coli_core', 'iCHOv1', 'iCN900', 'iEcDH1_1363', 'iIS312', 'iIT341', 'iJN1463', 'iJN678', 'iJR904', 'iLB1027_lipid', 'iLJ478', 'iMM1415', 'iPC815', 'iSbBS512_1146', 'iYS1720', 'Recon3D']


def get_networks_with_results():
	networks = list()
	directories = os.listdir(RESULTS_DIR)
	for dir_ in directories:
		if os.path.exists(f"{RESULTS_DIR}/{dir_}/results.json"):
			networks.append(dir_)
	return networks

def process(networks_list):
	results = dict()

	for network in networks_list:
		reader      = SBMLReader()
		document    = reader.readSBML(f'{DATABASE_DIR}/{network}.xml')
		model       = document.getModel()
		nb_metabo =  model.getNumSpecies()
		nb_reaction = model.getNumReactions()
		with open(f'{RESULTS_DIR}/{network}/results.json', 'r') as f:
			res = json.load(f)
		for tool in ['netseed','netseed_asp']:
			if tool == 'netseed': tool_name='Netseed'
			else: tool_name='NetSeed-ASP'
			results[f'{network}_{tool}'] = {
								'Number of metabolites': nb_metabo,
								'Number of reactions': nb_reaction,
								#'Execution time (sec)': res[tool]['time'],
								'Log of execution time': math.log(res[tool]['time'],10),
								'Tool': tool_name	
							}
	return results

def curve(data):
	#times = {'nb_react':[], 'nb_metabo':[], 'netseed':[], 'netseed_asp':[]}
	#react_times = {'nb_react':[], 'netseed':[], 'netseed_asp':[]}
	#for nwk in data:
	#	times['nb_metabo'].append(data[nwk]['metabolites'])
	#	times['nb_react'].append(data[nwk]['reactions'])
	#	for  tool in  data[nwk]['times']:
	#		times[tool].append(data[nwk]['times'][tool])
			#react_times[tool].append(data[nwk]['times'][tool])			

	#times = pd.DataFrame.from_dict(times)
	#print(times)
	#react_times = pd.DataFrame.from_dict(react_times)
	#times = times.drop([3, 20])
	#print(times)
	#print(react_times)

	data = pd.DataFrame.from_dict(data).T	
	print(data)	
	sns.set(style="whitegrid")

	plt.figure()
	sns_plot = sns.lineplot(data=data, x='Number of metabolites', y='Log of execution time', hue='Tool')
	plt.savefig("/beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results/metabolites.png", bbox_inches="tight")
	
	plt.figure()
	sns_plot = sns.lineplot(data=data, x='Number of reactions', y='Log of execution time', hue='Tool')
	plt.savefig("/beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results/reactions.png", bbox_inches="tight")

	"""
	f, (ax1, ax2) = plt.subplots(ncols=1, nrows=2, sharex=True, gridspec_kw={'height_ratios': [1, 10]})

	# we want the "Test" to appear on the x axis as individual parameters
	# "Latency in ms" should be what is shown on the y axis as a value
	# hue should be the "Experiment Setup"
	# this will result three ticks on the x axis with X1...X3 and each with three bars for T1...T3
	# (you could turn this around if you need to, depending on what kind of data you want to show)
	#ax1 = sns.lineplot(data=data, x='Number of metabolites', y='Execution time (sec)', hue='Tool',  ax=ax1)
	ax1 = sns.lineplot(data=data, x='Number of reactions', y='Execution time (sec)', hue='Tool',  ax=ax1)
	#ax1.lineplot(data=times, x='nb_metabo', y='netseed_asp',ax=ax1)

	# we basically do the same thing again for the second plot
	#ax2 = sns.lineplot(data=data, x='Number of metabolites', y='Execution time (sec)', hue='Tool',  ax=ax2)
	ax2 = sns.lineplot(data=data, x='Number of reactions', y='Execution time (sec)', hue='Tool',  ax=ax2)

	# here is the fun part: setting the limits for the individual y axis
	# the upper part (ax1) should show only values from 250 to 400
	# the lower part (ax2) should only show 0 to 150
	# you can define your own limits, but the range (150) should be the same so scale is the same across both plots
	# it could be possible to use a different range and then adjust plot height but who knows how that works
	ax1.set_ylim(160, 5900)
	ax2.set_ylim(0, 45)

	# the upper part does not need its own x axis as it shares one with the lower part
	ax1.get_xaxis().set_visible(False)

	# by default, each part will get its own "Latency in ms" label, but we want to set a common for the whole figure
	# first, remove the y label for both subplots
	ax1.set_ylabel("")
	ax2.set_ylabel("")
	# then, set a new label on the plot (basically just a piece of text) and move it to where it makes sense (requires trial and error)
	f.text(0.05, 0.55, 'Execution time (sec)', va='center', rotation='vertical')

	# by default, seaborn also gives each subplot its own legend, which makes no sense at all
	# soe remove both default legends first
	ax1.get_legend().remove()
	ax2.get_legend().remove()
	# then create a new legend and put it to the side of the figure (also requires trial and error)
	ax2.legend(loc=(1.025, 0.5), title="Tool")

	# let's put some ticks on the top of the upper part and bottom of the lower part for style
	ax1.xaxis.tick_top()
	ax2.xaxis.tick_bottom()

	# finally, adjust everything a bit to make it prettier (this just moves everything, best to try and iterate)
	f.subplots_adjust(left=0.15, right=0.85,wspace=0.2, 
                    hspace=0.05)

	#plt.savefig("/beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results/metabolites.png", bbox_inches="tight")
	plt.savefig("/beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results/reactions.png", bbox_inches="tight")

"""
"""
	
	f, (ax_top, ax_bottom) = plt.subplots(ncols=1, nrows=2, sharex=True, gridspec_kw={'hspace':0.05})


	sns.lineplot(data=times, x='nb_metabo', y='netseed', ax=ax_top)
	sns.lineplot(data=times, x='nb_metabo', y='netseed_asp', ax=ax_top)
	sns.lineplot(data=times, x='nb_metabo', y='netseed', ax=ax_bottom)
	sns.lineplot(data=times, x='nb_metabo', y='netseed_asp', ax=ax_bottom)	
	ax_top.set_ylim(bottom=5500)   # those limits are fake
	ax_bottom.set_ylim(0,200)
	
	sns.despine(ax=ax_bottom)
	sns.despine(ax=ax_top, bottom=True)

	ax = ax_top
	d = .015  # how big to make the diagonal lines in axes coordinates
	# arguments to pass to plot, just so we don't keep repeating them
	kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
	ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal

	ax2 = ax_bottom
	kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
	ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
	
	#remove one of the legend
	ax_bottom.get_legend().remove()
	ax_top.get_legend().remove()

	plt.xlabel('Metabolites number')
	plt.ylabel('Execution time (sec)')
	plt.title('Execution time in function of the number of metabolites')
	plt.legend(labels=['NetSeed', 'NetSeed-ASP'])	
	plt.savefig("/beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results/metabolites.png", bbox_inches="tight")
	
	#sns.lineplot(data=times, x='nb_react', y='netseed')
	#sns.lineplot(data=times, x='nb_react', y='netseed_asp')
	#plt.xlabel('Reactions number')
	#plt.ylabel('Execution time (sec)')
	#plt.title('Execution time in function of the number of reactions')
	#plt.legend(labels=['NetSeed', 'NetSeed-ASP'])
	#plt.savefig("/beegfs/mbolteau/seed_detection/computation_time_netseed_vs_netseed-asp/results/reactions.png",  bbox_inches="tight")

"""


if __name__ == '__main__':
	
	networks_list = get_networks_with_results()
	data = process(networks_list)
	curve(data)
