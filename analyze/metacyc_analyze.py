#!/usr/bin/env python


import os
import json
import sys
import csv
import random
import pathlib
from padmet.utils.sbmlPlugin import convert_from_coded_id
from matplotlib import pyplot as plt
import seaborn as sns
import pandas as pd
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.preprocessing import MultiLabelBinarizer
from libsbml import *


PATHWAYS_DIR='/beegfs/mbolteau/seed_detection/results/whole_network/topological_injection'
INPUTS_COMPOUNDS_DIR='/beegfs/mbolteau/seed_detection/databases/metacyc/pathways/all'

def presents(list1, list2):
	coded_list2 = [convert_from_coded_id(item)[0] for item in list2]	
	return all(elem in coded_list2 for elem in list1)

def equals(list1, list2):
	coded_list2 = [convert_from_coded_id(item)[0] for item in list2]
	coded_list2.sort()
	list1.sort()
	map(str.upper, coded_list2)
	map(str.upper, list2)
	return coded_list2 == list1

def get_cofactors_list(filename):
	cofactors = list()
	with open(filename, 'r') as f:
		reader = csv.reader(f, delimiter='\n')
		for row in reader:
			cofactors.append(row[0])
	return cofactors
def get_namefile_of_dir(dir):
	return os.listdir(dir)


def get_compounds(filename):
	document = readSBML(filename)
	compounds = set([cpd.getId() for cpd in document.getModel().getListOfSpecies()])
	return compounds



def confusion_matrix(all_, truth, pred):
	#print('ENTER')
	true_pos = pred.intersection(truth)
	true_neg = (all_.difference(pred)).intersection(all_.difference(truth))
	false_pos = pred.difference(truth)
	false_neg = (all_.difference(pred)).intersection(truth)
	return true_pos, true_neg, false_pos, false_neg

#({'A', 'B'}, {'G', 'H'}, {'C', 'D'}, {'C', 'D'})



def naive_analyze():
	pwy_list = get_namefile_of_dir(PATHWAYS_DIR)	
	
	data = dict()
	data['pathways_data'] = dict()
	data['pathways_number'] = 0
	data['missing_inputs_compounds'] = {'nb': 0, 'pathways':[]}
	data['netseed'] = 0
	data['netseed_asp'] = 0
	data['predator'] = 0
	data['predator_failed'] = {'nb': 0, 'pathways':[]}
	
	roc_data = dict()
	roc_data['truth'] = list()
	roc_data['netseed'] = list()
	roc_data['netseed_asp'] = list()
	roc_data['predator'] = list()

	for pwy in pwy_list:
		data['pathways_number'] += 1
		inputs_compounds_filename = INPUTS_COMPOUNDS_DIR+'/'+pwy+'/inputs_compounds.json'
		results = json.load(open(PATHWAYS_DIR+'/'+pwy+'/results.json', 'r'))
		data['pathways_data'][pwy] = results
		if os.path.isfile(inputs_compounds_filename):
			inputs_compounds = json.load(open(inputs_compounds_filename, 'r'))['inputs_compounds']
			roc_data['truth'].append(inputs_compounds)
			for tool in results.keys():
				solutions = results[tool]['solutions']
				inputs_compounds_presents = False
				for sol in solutions.values():
					if presents(inputs_compounds, sol): inputs_compounds_presents = True
				if inputs_compounds_presents : 
					data[tool] += 1
					roc_data[tool].append(sol)
				else:
					if len(solutions.values()) == 0:
						roc_data[tool].append(list())
					else:
						roc_data[tool].append(random.choice(list(solutions.values())))
				if tool == 'predator' and not inputs_compounds_presents:
					data['predator_failed']['nb'] +=1 
					data['predator_failed']['pathways'].append(pwy)				
		else: 
			data['missing_inputs_compounds']['nb']  += 1
			data['missing_inputs_compounds']['pathways'].append(pwy)
		
	with open('/beegfs/mbolteau/seed_detection/metacyc_analyse/results/naive_analyze.json', 'w') as outfile:
		json.dump(data, outfile, indent=4, sort_keys=True)

	return (data, roc_data)


def naive_ratio_analyze():
	pwy_list = get_namefile_of_dir(PATHWAYS_DIR)
	data = dict()
	data['pathways_data'] = dict()
	data['pathways_number'] = 0
	data['missing_inputs_compounds'] = {'nb': 0, 'pathways':[]}
	data['netseed'] = 0
	data['netseed_asp'] = 0
	data['predator'] = 0
	data['predator_failed'] = {'nb': 0, 'pathways':[]}

	for pwy in pwy_list:
		data['pathways_number'] += 1
		inputs_compounds_filename = INPUTS_COMPOUNDS_DIR+'/'+pwy+'/inputs_compounds.json'
		results = json.load(open(PATHWAYS_DIR+'/'+pwy+'/results.json', 'r'))
		data['pathways_data'][pwy] = results
		if os.path.isfile(inputs_compounds_filename):
			inputs_compounds = json.load(open(inputs_compounds_filename, 'r'))['inputs_compounds']
			for tool in results.keys():
				solutions = results[tool]['solutions']
				inputs_compounds_presents = list()
				nb_solutions = len(solutions.values())
				for sol in solutions.values():
					if presents(inputs_compounds, sol): inputs_compounds_presents.append(True)
				
				if len(inputs_compounds_presents) > 0 : data[tool] += len(inputs_compounds_presents)/nb_solutions
				if tool == 'predator' and not inputs_compounds_presents:
					data['predator_failed']['nb'] +=1
					data['predator_failed']['pathways'].append(pwy)
		else:
			data['missing_inputs_compounds']['nb']  += 1
			data['missing_inputs_compounds']['pathways'].append(pwy)

	with open('/beegfs/mbolteau/seed_detection/metacyc_analyse/results/naive_ratio_analyze.json', 'w') as outfile:
		json.dump(data, outfile, indent=4, sort_keys=True)

	return data


def strict_analyze():
	pwy_list = get_namefile_of_dir(PATHWAYS_DIR)

	data = dict()
	data['pathways_data'] = dict()
	data['pathways_number'] = 0
	data['missing_inputs_compounds'] = {'nb': 0, 'pathways':[]}
	data['netseed'] = {'score': 0, 'true_pos': 0, 'true_neg': 0, 'false_pos': 0, 'false_neg': 0, 'nb_sol':0}
	data['netseed_asp'] = {'score': 0, 'true_pos': 0, 'true_neg': 0, 'false_pos': 0, 'false_neg': 0, 'nb_sol':0}
	data['predator'] = {'score': 0, 'true_pos': 0, 'true_neg': 0, 'false_pos': 0, 'false_neg': 0, 'nb_sol':0}
	data['predator_failed'] = {'nb': 0, 'pathways':[]}

	roc_data = dict()
	roc_data['truth'] = list()
	roc_data['netseed'] = list()
	roc_data['netseed_asp'] = list()
	roc_data['predator'] = list()


	for pwy in pwy_list:
		data['pathways_number'] += 1
		inputs_compounds_filename = INPUTS_COMPOUNDS_DIR+'/'+pwy+'/inputs_compounds.json'
		results = json.load(open(PATHWAYS_DIR+'/'+pwy+'/results.json', 'r'))
		data['pathways_data'][pwy] = results
		if os.path.isfile(inputs_compounds_filename):
			inputs_compounds = json.load(open(inputs_compounds_filename, 'r'))['inputs_compounds']
			inputs_compounds_set = set(json.load(open(inputs_compounds_filename, 'r'))['inputs_compounds'])
			compounds_pwy_set = get_compounds(INPUTS_COMPOUNDS_DIR+'/'+pwy+'/'+pwy.lower()+'.sbml')
			roc_data['truth'].append(inputs_compounds)
			for tool in results.keys():
				solutions = results[tool]['solutions']
				inputs_compounds_presents = False
				true_pos = 0
				true_neg = 0
				false_pos = 0
				false_neg = 0
				for sol in solutions.values():
					data[tool]['nb_sol'] += 1
					sol_set = set([convert_from_coded_id(item)[0] for item in sol])
					compounds_pwy_set = set([convert_from_coded_id(item)[0] for item in compounds_pwy_set])
					if equals(inputs_compounds, sol): inputs_compounds_presents = True
					t_pos, t_neg, f_pos, f_neg = confusion_matrix(compounds_pwy_set, inputs_compounds_set, sol_set)  
					true_pos += len(t_pos)
					true_neg += len(t_neg)
					false_pos += len(f_pos)
					false_neg += len(f_neg)
				if len(solutions) != 0 : 
					data[tool]['true_pos'] += (true_pos/len(solutions))
					data[tool]['true_neg'] += (true_neg/len(solutions))
					data[tool]['false_pos'] += (false_pos/len(solutions))
					data[tool]['false_neg'] += (false_neg/len(solutions))
				if inputs_compounds_presents : 
					data[tool]['score'] += 1
					roc_data[tool].append(sol)
				else:
					if len(solutions.values()) == 0:
						roc_data[tool].append(list())
					else:
						roc_data[tool].append(random.choice(list(solutions.values())))
				if tool == 'predator' and not inputs_compounds_presents:
					data['predator_failed']['nb'] +=1
					data['predator_failed']['pathways'].append(pwy)
		else:
			data['missing_inputs_compounds']['nb']  += 1
			data['missing_inputs_compounds']['pathways'].append(pwy)

	with open('/beegfs/mbolteau/seed_detection/metacyc_analyse/results/strict_analyze__.json', 'w') as outfile:
		json.dump(data, outfile, indent=4, sort_keys=True)

	return (data, roc_data)


def strict_ratio_analyze():
        pwy_list = get_namefile_of_dir(PATHWAYS_DIR)
        data = dict()
        data['pathways_data'] = dict()
        data['pathways_number'] = 0
        data['missing_inputs_compounds'] = {'nb': 0, 'pathways':[]}
        data['netseed'] = 0
        data['netseed_asp'] = 0
        data['predator'] = 0
        data['predator_failed'] = {'nb': 0, 'pathways':[]}

        for pwy in pwy_list:
                data['pathways_number'] += 1
                inputs_compounds_filename = INPUTS_COMPOUNDS_DIR+'/'+pwy+'/inputs_compounds.json'
                results = json.load(open(PATHWAYS_DIR+'/'+pwy+'/results.json', 'r'))
                data['pathways_data'][pwy] = results
                if os.path.isfile(inputs_compounds_filename):
                        inputs_compounds = json.load(open(inputs_compounds_filename, 'r'))['inputs_compounds']
                        for tool in results.keys():
                                solutions = results[tool]['solutions']
                                inputs_compounds_presents = list()
                                nb_solutions = len(solutions.values())
                                for sol in solutions.values():
                                        if equals(inputs_compounds, sol): inputs_compounds_presents.append(True)

                                if len(inputs_compounds_presents) > 0 : data[tool] += len(inputs_compounds_presents)/nb_solutions
                                if tool == 'predator' and not inputs_compounds_presents:
                                        data['predator_failed']['nb'] +=1
                                        data['predator_failed']['pathways'].append(pwy)
                else:
                        data['missing_inputs_compounds']['nb']  += 1
                        data['missing_inputs_compounds']['pathways'].append(pwy)

        with open('/beegfs/mbolteau/seed_detection/metacyc_analyse/results/strict_ratio_analyze.json', 'w') as outfile:
                json.dump(data, outfile, indent=4, sort_keys=True)

        return data



def free_currency_metabolites_analyze(cofactors_filename):
	pwy_list = get_namefile_of_dir(PATHWAYS_DIR)
	cofactors_list = get_cofactors_list(cofactors_filename)

	data = dict()
	data['pathways_data'] = dict()
	data['pathways_number'] = 0
	data['missing_inputs_compounds'] = {'nb': 0, 'pathways':[]}
	data['netseed'] = 0
	data['netseed_asp'] = 0
	data['predator'] = 0
	data['predator_failed'] = {'nb': 0, 'pathways':[]}

	for pwy in pwy_list:
		data['pathways_number'] += 1
		inputs_compounds_filename = INPUTS_COMPOUNDS_DIR+'/'+pwy+'/inputs_compounds.json'
		results = json.load(open(PATHWAYS_DIR+'/'+pwy+'/results.json', 'r'))
		data['pathways_data'][pwy] = results
		if os.path.isfile(inputs_compounds_filename):
			inputs_compounds = json.load(open(inputs_compounds_filename, 'r'))['inputs_compounds']
			curated_inputs_compounds = list()
			for cpd in inputs_compounds:
				if cpd not in cofactors_list: curated_inputs_compounds.append(cpd)
			for tool in results.keys():
				solutions = results[tool]['solutions']
				inputs_compounds_presents = False
				for sol in solutions.values():
					if presents(curated_inputs_compounds, sol): inputs_compounds_presents = True
				if inputs_compounds_presents : data[tool] += 1
				if tool == 'predator' and not inputs_compounds_presents:
					data['predator_failed']['nb'] +=1
					data['predator_failed']['pathways'].append(pwy)
		else:
			data['missing_inputs_compounds']['nb']  += 1
			data['missing_inputs_compounds']['pathways'].append(pwy)

	with open('/beegfs/mbolteau/seed_detection/metacyc_analyse/results/free_currency_metabolites_analyze.json', 'w') as outfile:
		json.dump(data, outfile, indent=4, sort_keys=True)

	return data




def computation_times_analyze(data):
	times = dict()
	times['netseed'] = list()
	times['netseed_asp'] = list()
	times['predator'] = list()
	for pwy in data['pathways_data']:
		for tool in data['pathways_data'][pwy].keys():
			time = data['pathways_data'][pwy][tool]['time']
			times[tool].append(time)
	
	times = pd.DataFrame.from_dict(times)
		
	fig, ax = plt.subplots()
	
	# ax.violinplot([times['netseed'], times['netseed_asp'], times['predator']], showmeans=False, showmedians=True, vert=False)
	
	ax.set(xlabel='Time (sec)', ylabel='Program', title='Computational time comparison')
	
	sns.set(style="whitegrid")
	sns.violinplot(data=times, ax=ax, orient='h')
	plt.savefig("/beegfs/mbolteau/seed_detection/metacyc_analyse/results/plot.png",  bbox_inches="tight")
		

def roc_curve_creation(data):
	f_pos_netseed, t_pos_netseed, _= roc_curve(data['truth'], data['netseed'])
	plt.subplots(1, figsize=(10,10))
	plt.title('Receiver Operating Characteristic - DecisionTree')
	plt.plot(f_pos_netseed, t_pos_netseed)
	plt.plot([0, 1], ls="--")
	plt.plot([0, 0], [1, 0] , c=".7"), plt.plot([1, 1] , c=".7")
	plt.ylabel('True Positive Rate')
	plt.xlabel('False Positive Rate')
	plt.savefig('roc.png')

	

if __name__ == '__main__':

	cofactors_filename = '/beegfs/mbolteau/seed_detection/metacyc_analyse/code/all_cofactors.csv'


	# naive_analyze_data = naive_analyze()

	#naive_ratio_analyze_data = naive_ratio_analyze()
	
	#print(confusion_matrix(set(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']), set(['A', 'B', 'E', 'F']), set(['A', 'B', 'C', 'D'])))
	
	strict_analyze_data, strict_analyze_roc_data = strict_analyze()


	#strict_ratio_analyze_data = strict_ratio_analyze()
	# free_currency_metabolites_data  = free_currency_metabolites_analyze(cofactors_filename)

	# computation_times_analyze(naive_analyze_roc_data_binarized)

	# roc_curve_creation(strict_analyze_roc_data_binarized)
