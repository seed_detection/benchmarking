#!/usr/bin/env python

from libsbml import *
from math import ceil
import random
import os
import sys

INPUT_FILE                  = sys.argv[1]
OUTPUT_FILE                 = sys.argv[2]
POURCENT_DEGRADATION        = float(sys.argv[3])

def degrade():
    reader      = SBMLReader()
    document    = reader.readSBML(INPUT_FILE)
    model       = document.getModel()
    level       = document.getLevel()
    version     = document.getVersion()



    model_copy = model.clone()
    rxn_list = model_copy.getListOfReactions()
    size = rxn_list.size()    
    rxn_nb_to_del = int(ceil(size*POURCENT_DEGRADATION)) # Round up
    for j in range(rxn_nb_to_del):
        rxn_list.remove(random.choice(rxn_list).getId())
    #doc = SBMLDocument(level, version)
    #doc.setModel(model_copy)      
    #writeSBMLToFile(doc, OUTPUT_FILE)
    document_copy =  document.clone()
    sbml_str = document_copy.toSBML()
    with open(OUTPUT_FILE, 'w') as outfile:
                outfile.write("<?xml version='1.0' encoding='UTF-8'?>\n")
                outfile.write(sbml_str)

if __name__=='__main__':
    degrade()
