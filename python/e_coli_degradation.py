#!/usr/bin/env python

from libsbml import *
from math import ceil
import random
import os
import sys
from get_biomass_reaction_name import get_biomass_reaction_name


INPUT_FILE                  = sys.argv[1]
OUTPUT_DIRECTORY            = sys.argv[2]
POURCENT_DEGRADATION_LIST   = [2, 5, 10, 15]


def no_reactants_or_no_products(rxn):
    reactants = rxn.getListOfReactants()
    products = rxn.getListOfProducts()
    if (len(reactants) == 0) or (len(products) == 0): 
        return True
    return False

def get_ex_biomass_reaction(rxn_list, biomass_reaction_name, level, version):
    ex_biomass_rxn_list = list()
    for rxn in rxn_list:
        if rxn.getId() ==  biomass_reaction_name or 'R_EX' in rxn.getId() or no_reactants_or_no_products(rxn):
            ex_biomass_rxn_list.append(rxn.getId())
    return ex_biomass_rxn_list



def degrade():
    os.mkdir(OUTPUT_DIRECTORY)
    reader      = SBMLReader()
    document    = reader.readSBML(INPUT_FILE)
    model       = document.getModel()
    level       = document.getLevel()
    version     = document.getVersion()
    

    biomass_reaction_name = get_biomass_reaction_name(INPUT_FILE)
   
    pourcent_degradation_list = [x/100 for x in POURCENT_DEGRADATION_LIST]
    
    for pct in pourcent_degradation_list:
        os.mkdir(f'{OUTPUT_DIRECTORY}/{pct}')    

        for i in range(100):
            model_copy = model.clone()
            rxn_list = model.getListOfReactions()
            ex_biomass_rxn_list = get_ex_biomass_reaction(rxn_list, biomass_reaction_name, level, version)
            size = rxn_list.size()    
            rxn_nb_to_del = int(ceil(size*pct)) # Round up
            for j in range(rxn_nb_to_del):
                rxn_list = model_copy.getListOfReactions()
                id_ = random.choice(rxn_list).getId()
                while id_ in ex_biomass_rxn_list: id_ = random.choice(rxn_list).getId()

                model_copy.removeReaction(id_)
            
            document_copy =  document.clone()
            document_copy.setModel(model_copy)
            sbml_str = document_copy.toSBML() 
            with open(f'{OUTPUT_DIRECTORY}/{pct}/{pct}_{i+1}.sbml', 'w') as outfile:
                outfile.write("<?xml version='1.0' encoding='UTF-8'?>\n")
                outfile.write(sbml_str)

if __name__=='__main__':
    degrade()
