#!/usr/bin/env python

from libsbml import *
import sys

def get_biomass_reaction_name(filename):

    reader      = SBMLReader()
    document    = reader.readSBML(filename)
    model       = document.getModel()
    level       = document.getLevel()
    version     = document.getVersion()
    model_fbc   = model.getPlugin("fbc")
    obj_list    = (model_fbc.getListOfObjectives())
    obj_id      = obj_list.getActiveObjective()
    obj         = model_fbc.getObjective(obj_id)
    for flux_obj in obj.getListOfFluxObjectives():
        biomass_reaction_name = flux_obj.getReaction()

    return biomass_reaction_name


if __name__ == '__main__':
    filename = sys.argv[1]
    print(get_biomass_reaction_name(filename))
