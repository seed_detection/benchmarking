#!/usr/bin/env python

from libsbml import *
import sys
import random
from get_biomass_reaction_name import get_biomass_reaction_name


def get_cpds(filename, nb_cpds):
	reader      = SBMLReader()
	document    = reader.readSBML(filename)
	model       = document.getModel()
	biomass_name = get_biomass_reaction_name(filename)
	biomass_rxn = model.getReaction(biomass_name)
	reactants = [rct.getSpecies() for rct in biomass_rxn.getListOfReactants()]
	if len(reactants) > nb_cpds:
		cpds = random.sample(reactants, nb_cpds)
	else:
		cpds = reactants
	print(cpds)	
	return cpds


if __name__ == '__main__':

	filename = sys.argv[1]
	nb_cpds = sys.argv[2]
	get_cpds(filename, int(nb_cpds))
