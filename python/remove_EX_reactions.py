#!/usr/bin/env python

import sys
from libsbml import *


def get_ex_reactions(rxn_list, level, version):
    ex_rxn_list = list()
    for rxn in rxn_list:
        if 'R_EX' in rxn.getId():
            ex_rxn_list.append(rxn.getId())
    return ex_rxn_list

def no_reactants_or_no_products(rxn):
    reactants = rxn.getListOfReactants()
    products = rxn.getListOfProducts()
    if (len(reactants) == 0) or (len(products) == 0): 
        return True
    return False

def remove_EX_reactions(input_file, output_file):
    reader      = SBMLReader()
    document    = reader.readSBML(input_file)
    namespaces  = document.getNamespaces()
    model       = document.getModel()
    level       = document.getLevel()
    version     = document.getVersion()

    model_copy = model.clone()
    rxn_list = model.getListOfReactions()
    ex_rxn_list = get_ex_reactions(rxn_list, level, version)
    
    for rxn in rxn_list:
        print(rxn.getId())
        rxn_id = rxn.getId()
        
        if rxn_id in ex_rxn_list or no_reactants_or_no_products(rxn): 
            model_copy.removeReaction(rxn_id)

    document_copy = document.clone()
    document_copy.setModel(model_copy)
    sbml_str = document_copy.toSBML() 
    with open(output_file, 'w') as outfile:
        outfile.write("<?xml version='1.0' encoding='UTF-8'?>\n")
        outfile.write(sbml_str)
    


if __name__=='__main__':
    input_file = sys.argv[1]
    output_file = sys.argv[2]
    remove_EX_reactions(input_file, output_file)
